# Trainer Base

[![PyPI version](https://badge.fury.io/py/TrainerBase.svg)](https://badge.fury.io/py/TrainerBase)
[![Downloads](https://static.pepy.tech/badge/trainerbase)](https://pepy.tech/project/trainerbase)

## To create project from template using cookiecutter

```bash
cookiecutter https://gitlab.com/v01d-gl/basic-project-template.git --checkout trainerbase
```

## Examples

[Python Trainers](https://gitlab.com/python-trainers)
